from usercom.wsgi import *
import csv
from itertools import islice
from bs4 import BeautifulSoup
import lxml.html
import time
import requests
from task.models import Website, WebsiteCategory, WebPage
from django.db.models import Count


def find_description(metas):
    for meta in metas:
        if 'name' in meta.attrs and meta.attrs['name'] == 'description':
            return meta.attrs['content']


def get_meta_and_title(url):
    try:
        r = requests.get(url, timeout=2)
        tree = lxml.html.fromstring(r.content)
        found_title = tree.findtext('.//title')
        soup = BeautifulSoup(r.text)
        metas = soup.find_all('meta')
        description = find_description(metas)
    except Exception as e:
        print("Couldn't find title due to:", e)
        found_title = "Title not found"
        description = "Description not found"
    return found_title, description


def get_category(url):
    try:
        r = requests.get('https://www.alexa.com/siteinfo/' + url)
        soup = BeautifulSoup(r.text, 'lxml')
        category = soup.select_one('div[class^="subText truncation"]').text
    except Exception as e:
        print("Category couldn't be find due to: ", e)
        category = "Category not known"
    return category


def save_data_to_csv(csv_writer, line, found_title, category, description):
    try:
        csv_writer.writerow([line[0], line[1], found_title, category, description])
    except UnicodeEncodeError as e:
        print("Couldn't save data due to:", e)
        found_title = found_title.encode('utf-8')
        csv_writer.writerow([line[0], line[1], found_title, category, description])
    except:
        found_title = "No title"
        description = "No description"
        csv_writer.writerow([line[0], line[1], found_title, category, description])


def find_data():
    with open('top-1m.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)

        with open('top.csv', 'w') as new_file:
            csv_writer = csv.writer(new_file, delimiter=',')

            for line in islice(csv_reader, 0, 100):
                url = line[1]
                proper_url = "http://" + url + "/"
                found_title, description = get_meta_and_title(proper_url)
                category = get_category(url)
                save_data_to_csv(csv_writer, line, found_title, category, description)
                print(line[0], line[1], found_title, category, description)


def create_categories_model():
    with open('top.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for line in csv_reader:
            if line:
                category = line[3]
                cat_desc = "Websites about " + category
                created = WebsiteCategory.objects.get_or_create(
                    name=category,
                    description=cat_desc,
                )


def create_website_model():
    with open('top.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for line in csv_reader:
            if line:
                url = line[1]
                title = line[2]
                meta = line[4]
                rank = int(line[0])
                cat = line[3]
                created = Website.objects.get_or_create(
                    url=url,
                    title=title,
                    meta=meta,
                    alexa_rank=rank,
                    category=WebsiteCategory.objects.get(name=cat),
                )
        WebsiteCategory.objects.annotate(count=Count('website'))

def create_webpage_model():
    with open('top.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for line in csv_reader:
            if line:
                url = line[1]
                title = line[2]
                meta = line[4]
                created = WebPage.objects.get_or_create(
                    website = Website.objects.get(url=url),
                    URL=url,
                    title=title,
                    meta_description=meta,
                )


# def delete_objects():
#     # WebsiteCategory.objects.all().delete()
#     # Website.objects.all().delete()

if __name__=="__main__":
    # find_data()
    # create_categories_model()
    # create_website_model()
    print(list(WebsiteCategory.objects.all().values('name')))