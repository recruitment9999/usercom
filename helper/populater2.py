import csv
from itertools import islice
from bs4 import BeautifulSoup
import lxml.html
import time
# import requests
# from task.models import Website
import grequests


def find_description(metas):
    for meta in metas:
        if 'name' in meta.attrs and meta.attrs['name'] == 'description':
            return meta.attrs['content']


def exception(request, exception):
    print("Problem: {}: {}".format(request.url, exception))


with open('top-1m.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    with open('top.csv', 'w') as new_file:
        csv_writer = csv.writer(new_file, delimiter=',')
        proper_urls = []
        for u in islice(csv_reader, 0, 10000):
            proper_urls.append("'http://" + u[1] + "/'")

        rs = (grequests.get(u) for url in proper_urls)
        results = grequests.map(rs, exception_handler=exception)
        print(results)
