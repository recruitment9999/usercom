from django.shortcuts import render
from django.views.generic import ListView, DetailView
from .models import WebPage, WebsiteCategory, Website



# Create your views here.


def index(request):
    context = {'websites': Website.objects.all()}
    return render(request, 'task/home.html', context)


class WebsiteListView(ListView):
    model = Website
    template_name = 'task/home.html'
    context_object_name = 'websites'
    paginate_by = 5


def categories_view(request):
    context = {'categories': WebsiteCategory.objects.all()}
    return render(request, 'task/category.html', context)


def category_detail(request, name):
    context = {'websites': Website.objects.filter(category__name__contains=name)}
    return render(request, 'task/details.html', context)


class WebsiteDetailView(DetailView):
    model = Website


def about(request):
    return render(request, 'task/about.html')