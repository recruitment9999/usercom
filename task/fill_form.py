from .form import MyForm
from django.shortcuts import render, redirect
from .models import WebsiteCategory, Website


def category_chooser(request):
    cat_form = MyForm()
    if request.method == 'POST':
        cat_form = MyForm(request.POST)
        if cat_form.is_valid():
            cd = cat_form.cleaned_data
            choosen_cat = cd.get('category')
            data_to_render = Website.objects.filter(category__name__contains=choosen_cat)
            return render(request, 'task/details.html', {'websites': data_to_render})
    return render(request, "task/home.html", {'websites': Website.objects.all(), 'category_chooser': cat_form})
    #
    # context = {'websites': Website.objects.all()}
    # return render(request, 'task/home.html', context)
