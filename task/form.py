from usercom.wsgi import *
from django import forms
from task.models import WebsiteCategory


categories = list(WebsiteCategory.objects.all().values('name'))
final_list = []
for position in categories:
    for key in position:
        final_list.append((position[key], position[key]))


class MyForm(forms.Form):
    category = forms.ChoiceField(choices=final_list, label="Please choose desired category", initial='', widget=forms.Select())


if __name__=="__main__":
    print(list(WebsiteCategory.objects.all().values('name')))
    print(final_list)
