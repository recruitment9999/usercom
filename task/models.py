from django.db import models
from django.utils import timezone
# Create your models here.


class WebsiteCategory(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    # count = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Website(models.Model):
    url = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    meta = models.CharField(max_length=500)
    alexa_rank = models.IntegerField()
    category = models.ForeignKey(WebsiteCategory, on_delete=models.CASCADE)

    def __str__(self):
        return self.url


class WebPage(models.Model):
    website = models.ForeignKey(Website, on_delete=models.CASCADE)
    URL = models.CharField(max_length=255, unique=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=100)
    meta_description = models.TextField

    def __str__(self):
        return self.website.title
