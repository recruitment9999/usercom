from django.urls import path, include
from .views import WebsiteListView, WebsiteDetailView
from . import views, fill_form
from django.conf.urls import include

urlpatterns = [
    # path('', WebsiteListView.as_view(), name='index'),
    path('', fill_form.category_chooser, name='index'),
    # path('/<str:webiste>', UserPostListView.as_view(), name='user-posts'),
    path('post/<str:website>/', WebsiteDetailView.as_view(), name='website-detail'),
    # path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    # path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete'),
    # path('post/new/', PostCreateView.as_view(), name='post-create'),
    path('categories', views.categories_view, name="categories-view"),
    path('categories/<str:name>/', views.category_detail, name="category-detail")
    # path('planner/', fill_form.planner, name="planner"),
    # path('hello/', views.hello, name='hello'),

]